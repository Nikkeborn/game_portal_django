from django.contrib import admin
from .models import *


# Define the admin class
class GameAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'category', 'publisher', 'created_date', 'published_date', 'edited_date')
    list_filter = ('created_date', 'category', 'publisher')


class PublisherAdmin(admin.ModelAdmin):
    list_display = ('name', 'ceo_name', 'country', 'foundation_date')


class PictureAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'game')


# Register the admin class with the associated model
admin.site.register(Game, GameAdmin)
admin.site.register(Publisher, PublisherAdmin)
admin.site.register(Picture, PictureAdmin)


admin.site.register(Category)
# admin.site.register(Publisher)
# admin.site.register(Game)
# admin.site.register(Picture)

from django.urls import path
from .views import *


app_name = "catalog"
# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('catalog/game/', GameListView.as_view(), name='games'),
    path('catalog/game/<str:slug>/', GameDetailView.as_view(), name='game_details'),
    path('catalog/category/', CategoryListView.as_view(), name='categories'),
    path('catalog/category/<int:pk>/', CategoryDetailView.as_view(), name='category_details'),
    path('catalog/publisher/', PublisherListView.as_view(), name='publishers'),
    path('catalog/publisher/<int:pk>/', PublisherDetailView.as_view(), name='publisher_details'),
    path('catalog/picture/', picture_list, name='pictures'),
    path('catalog/picture/<int:pk>/', picture_detail, name='picture_details'),
]
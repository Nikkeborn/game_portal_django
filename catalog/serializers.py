from rest_framework import serializers
from .models import *


class CategorySerializer(serializers.ModelSerializer):
    games = serializers.StringRelatedField(many=True)

    class Meta:
        model = Category
        fields = '__all__'


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publisher
        fields = ('name', 'ceo_name', 'country', 'foundation_date', 'games')


class GameSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=200)
    description = serializers.CharField(max_length=800, required=False, allow_blank=True)
    full_description = serializers.CharField(required=False, allow_blank=True)
    slug = serializers.SlugField(max_length=400)
    category = serializers.CharField(required=False)
    publisher = serializers.CharField(required=False)
    published_date = serializers.DateTimeField(required=False)
    picture_url_list = serializers.SerializerMethodField('get_picture_url_list')

    def get_picture_url_list(self, game_obj, *args, **kwargs):
        # print(args)
        # print(kwargs)
        # print(type(args), args)
        print(type(game_obj), game_obj)
        # print(game_obj.get_picture_set())
        picture_set = game_obj.get_picture_set()
        url_list = []
        for pic in picture_set:
            url_list.append(pic.url)
        return url_list

    def create(self, validated_data):
        """
        Create and return a new `Game` instance, given the validated data.
        """
        # print(validated_data)
        # print(type(validated_data))
        try:
            category = Category.objects.get(name=validated_data['category'])
            validated_data['category'] = category
            # print(type(category))
            # print(category)
        except Exception:
            validated_data['category'] = None

        try:
            publisher = Publisher.objects.get(name=validated_data['publisher'])
            validated_data['publisher'] = publisher
            # print(type(publisher))
            # print(publisher)
        except Exception:
            validated_data['publisher'] = None

        return Game.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Game` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.full_description = validated_data.get('full_description', instance.full_description)
        instance.slug = validated_data.get('slug', instance.slug)
        instance.category = validated_data.get('category', instance.category)
        instance.publisher = validated_data.get('publisher', instance.publisher)
        instance.save()
        return instance


class PictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Picture
        fields = ('id', 'title', 'url', 'game')

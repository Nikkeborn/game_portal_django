from django.db import models
from django.utils import timezone
from django.shortcuts import reverse


class Category(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class GameSetManager(models.Manager):
    def game_set(self):
        set = self.games.all()
        return set


class Publisher(models.Model):
    name = models.CharField(max_length=200, unique=True)
    ceo_name = models.CharField(max_length=200, db_index=True, blank=True)
    country = models.CharField(max_length=200, blank=True)
    foundation_date = models.DateField(blank=True, null=True)
    objects = models.Manager()
    games = GameSetManager()

    def __str__(self):
        return self.name


class Game(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    description = models.CharField(max_length=800, db_index=True, blank=True)
    full_description = models.TextField(db_index=True, blank=True)
    slug = models.SlugField(max_length=400, unique=True)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete = models. SET_NULL, related_name='games')
    publisher = models.ForeignKey(Publisher, blank=True, null=True, on_delete = models. SET_NULL, related_name='games')
    created_date = models.DateTimeField(auto_now_add=True)
    published_date = models.DateTimeField(default=timezone.now, blank=True, null=True)
    edited_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_picture_set(self):
        picture_set = self.pictures.all()
        return picture_set


class Picture(models.Model):
    title = models.CharField(max_length=200, db_index=True, blank=True)
    url = models.URLField()
    game = models.ForeignKey(Game, blank=True, null=True, on_delete=models.CASCADE, related_name='pictures')

    def __str__(self):
        return self.title
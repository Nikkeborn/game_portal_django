from django.contrib.auth.models import User
from rest_framework import mixins
from rest_framework import generics

from rest_framework import status
from rest_framework.response import Response

from .serializers import *


class UserListView(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        # print(serializer.validated_data)
        password = serializer.validated_data['password']
        # print(password)
        user = serializer.save()
        # print(user.__dict__)
        # print(serializer.save(), type(serializer.save()))
        # print(user, type(user))
        user.set_password(password)
        user.save()
        # print(user, type(user))


class UserDetailView(mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin, generics.GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


